
/**
 *
 * @author Viktor
 */
public class ClausolaRisolventeCondivisa extends ClausolaRisolvente{
	private final String creatore;
	int numLettureEseguite = 0;
	
	public ClausolaRisolventeCondivisa(ClausolaRisolvente c, String processo_creatore){
		this.letterali = c.letterali;
		this.hashLetterali = c.hashLetterali;
		creatore = processo_creatore;
		this.padre1 = c.padre1;
		this.padre2 = c.padre2;
	}
	
	public String getProcessoCreatore(){
		return creatore;
	}
}
