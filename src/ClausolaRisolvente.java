
/**
 *
 * @author Viktor
 */
public class ClausolaRisolvente extends Clausola{
    Clausola padre1;
    Clausola padre2;
    
    public ClausolaRisolvente(Clausola c){
        letterali = c.letterali;
		hashLetterali = c.hashLetterali;
    }
    
    public ClausolaRisolvente(){
        super();
    }
    
    public void setPadri(Clausola c1, Clausola c2){
        padre1 = c1;
        padre2 = c2;
    }
   
	
	@Override
    public boolean haGenitori(){
        return padre1 != null && padre2 != null;
    }
	
	public boolean equals(ClausolaRisolvente c){
		//if(this.padre1 == null || this. padre2 == null || c.getPadre1() == null || c.getPadre2() == null)
		return super.equals(c);
		
		//return super.equals(c) && this.padre1.equals(c.getPadre1()) && this.padre2.equals(c.getPadre2());
	}
}
