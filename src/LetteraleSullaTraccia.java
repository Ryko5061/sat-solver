
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Viktor
 */
public class LetteraleSullaTraccia extends Letterale{
    final int tipo;
    public static final int DECISO = 1;
    public static final int PROPAGATO = 2;
    private Clausola giustificazione;
    private final int livello_aggiunta;
    
    public LetteraleSullaTraccia(int letterale, int tipo, int livello) {
        super(letterale);
        controllaTipo(tipo);
        this.tipo = tipo;
        livello_aggiunta = livello;
    }
	
	public LetteraleSullaTraccia(int letterale, int tipo, int livello, Clausola giustificazione) {
        super(letterale);
        controllaTipo(tipo);
        this.tipo = tipo;
        livello_aggiunta = livello;
		this.giustificazione = giustificazione;
    }
	
	
    
    public LetteraleSullaTraccia(Letterale l, int livello, Clausola giustificazione){
        super(l.getValore());
        livello_aggiunta = livello;
        tipo = LetteraleSullaTraccia.PROPAGATO;
        this.giustificazione = giustificazione;    
    }
	
	public LetteraleSullaTraccia(int l, int livello, Clausola giustificazione){
        super(l);
        livello_aggiunta = livello;
        tipo = LetteraleSullaTraccia.PROPAGATO;
        this.giustificazione = giustificazione;    
    }
	
	public LetteraleSullaTraccia(Integer l, int livello, Clausola giustificazione){
        super(l);
        livello_aggiunta = livello;
        tipo = LetteraleSullaTraccia.PROPAGATO;
        this.giustificazione = giustificazione;    
    }
    
    public LetteraleSullaTraccia(Letterale l, int tipo, int livello) {
        super(l.getValore());
        controllaTipo(tipo);
        this.tipo = tipo;
        livello_aggiunta = livello;
    }
    
    public Clausola getGiustificazione(){
        return giustificazione;
    }
    
    public int getLivello(){
        return livello_aggiunta;
    }
    
    private void controllaTipo(int tipo){
        switch(tipo){
            case DECISO:
            case PROPAGATO: break;
        }
    }
    
}
