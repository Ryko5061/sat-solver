import java.awt.*;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Viktor
 */
public class GUI extends javax.swing.JFrame {
    private final RecuperoFile chooser;
    private final Core core1, core2, core3;
    private boolean computazione_completata = false;
	private boolean risultato_ricevuto = false;
	private Thread t1, t2, t3;
	private boolean risultato;
	private double tempoComputazione;
	private double tempoProva;
	private String processoRisultato;
	private int progresso = 0;
    
    public GUI() {
        initComponents();
		infoLabel.setText("");
		infoLabel2.setText("");
		jRadioButton1.setVisible(false);
		jRadioButton2.setVisible(false);
        startButton.setEnabled(false);
        core1 = new Core(this);
		core2 = new Core(this);
		core3 = new Core(this);
        chooser = new RecuperoFile(this);
        chooser.setCores(core1 , core2, core3);
        chooser.setVisible(false);
        startButton.setEnabled(false);
        infoLabel.setText("");
        this.setMinimumSize(new Dimension(750,400));  
		buttonGroup1.add(vsidsRadioButton);
		buttonGroup1.add(greedyRadioButton);
		buttonGroup1.add(bothRadioButton);
		buttonGroup1.add(scfRadioButton);
		vsidsRadioButton.setSelected(true);
		buttonGroup2.add(jRadioButton1);
		buttonGroup2.add(jRadioButton2);
		jRadioButton2.setSelected(true);
		jProgressBar1.setStringPainted(true);
		jProgressBar1.setVisible(false);
		jProgressBar1.setMaximum(0);
		//greedyRadioButton.setVisible(false);
		//scfRadioButton.setVisible(false);
    }
	
	public void setFileName(String s){
		fileNameLabel.setText("File selezionato: "+s);
	}
    
    public void activateStartButton(){
        startButton.setEnabled(true);
    }
	
	public boolean mostraProvaAttivo(){
		return provaCheckBox.isSelected();
	}
    
    public void impossibileLeggereIlFile(){
		java.awt.EventQueue.invokeLater(() -> {
			infoLabel.setText("Impossibile leggere il file");
			infoLabel.setBackground(Color.red);
		});
    }
    
    public void computazioneCompletata(){
		StringBuilder mex;
		mex = new StringBuilder("Computazione completata in ");
		if(tempoComputazione/1000000 < 1000){
				
				mex.append(new DecimalFormat("###.##").format(tempoComputazione/1000000));
				mex.append(" ms. Il problema \u00E8 ");
				mex.append(risultato ? "soddisfacibile." : "insoddisfacibile. ");
		}
		else if(tempoComputazione/1000000000 < 60){
			mex.append(new DecimalFormat("##.##").format(tempoComputazione/1000000000));
			mex.append(" s. Il problema \u00E8 ");
			mex.append(risultato ? "soddisfacibile." : "insoddisfacibile. ");
		}
		else if((tempoComputazione/1000000000)/60 < 60){
			mex.append(Math.floor((int) (tempoComputazione/1000000000)/60));
			mex.append(" min e ");
			mex.append(new DecimalFormat("##").format(((tempoComputazione/1000000000)/60 - Math.floor((tempoComputazione/1000000000)/60))*60));
			mex.append(" s. Il problema \u00E8 ");
			mex.append(risultato ? "soddisfacibile." : "insoddisfacibile. ");
		}
		else{ 
			mex.append((int) Math.floor((tempoComputazione/1000000000)/60/60));
			mex.append(" ore e ");
			mex.append(new DecimalFormat("##").format(((tempoComputazione/1000000000)/60/60 - Math.floor((tempoComputazione/1000000000)/60/60))*60));
			mex.append(" min. Il problema \u00E8 ");
			mex.append(risultato ? "soddisfacibile." : "insoddisfacibile, ");
		}
		
		final String messaggio = mex.toString();
		java.awt.EventQueue.invokeLater(() -> infoLabel.setText(messaggio));
		computazione_completata = true;
    }
	
	private void generazioneRisultatoCompletata(int dimensioneTracciaOProva){
		if(infoLabel.getText().equals("Computazionein corso...")){
			return;
		}
		StringBuilder mex = new StringBuilder("<html>");
		mex.append("<br>\u00C9 stato impiegato ");
		if(tempoProva/1000000 < 1000){
			mex.append(new DecimalFormat("###.##").format(tempoProva/1000000));
			mex.append(" ms");
		}
		else if(tempoProva/1000000000 < 60){
			mex.append(new DecimalFormat("##.##").format(tempoProva/1000000000));
			mex.append(" s");
		}
		else if((tempoProva/1000000000)/60 < 60){
			mex.append( (int) Math.floor((tempoProva/1000000000)/60));
			mex.append(" min e ");
			mex.append(new DecimalFormat("##").format(((tempoProva/1000000000)/60 - Math.floor((tempoProva/1000000000)/60))*60));
			mex.append(" s");
		}
		else{ 
			mex.append((int) Math.floor((tempoProva/1000000000)/60/60));
			mex.append(" ore e ");
			mex.append(new DecimalFormat("##").format(((tempoProva/1000000000)/60/60 - Math.floor((tempoProva/1000000000)/60/60))*60));
			mex.append(" min");
		}
		mex.append(" per la generazione ");
		mex.append(risultato ? "della traccia" : "delle "+dimensioneTracciaOProva+" risoluzioni della prova");		
		mex.append("</html>");
		final String messaggio = mex.toString();
		java.awt.EventQueue.invokeLater(() -> infoLabel2.setText(messaggio));
	}
    
    public void computazioneNonCompletata(){
        computazione_completata = false;
    }
    
    public void riceviRisultato(boolean soddisfatto, double tempoComputazione, String processo){
		if(!risultato_ricevuto){
			risultato_ricevuto = true;
			risultato = soddisfatto;
			this.tempoComputazione = tempoComputazione;
			if(bothRadioButton.isSelected()){
				switch (processo) {
					case "t1":
						core2.interrompi();
						core3.interrompi();
						try {
							t2.interrupt();
							t3.interrupt();
							t2.join();
							t3.join();
						} catch (InterruptedException ex) {
							Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
						}


						break;
					case "t2":

						core1.interrompi();
						core3.interrompi();
						try {
							t1.interrupt();
							t3.interrupt();
							t1.join();
							t3.join();
						} catch (InterruptedException ex) {
							Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
						}
						break;
					default:

						core1.interrompi();
						core2.interrompi();
						try {
							t2.interrupt();
							t1.interrupt();
							t2.join();
							t1.join();
						} catch (InterruptedException ex) {
							Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
						}

						break;
				}
				
			}
			computazione_completata = true;
			sbloccaPulsanti();
			processoRisultato = processo;
		}
		
    }
	
	public void riceviTraccia(String traccia, double tempoProva){
		outputArea.setText(traccia);
		this.tempoProva = tempoProva;
		generazioneRisultatoCompletata(0);
		sbloccaPulsanti();
	}
	
	public void riceviProva(LinkedList<String> prova, double tempoProva){
		if(prova.size() < 1000){
			for(String s: prova){
				java.awt.EventQueue.invokeLater(() -> outputArea.append(s));
			}
		}
		else{
			outputArea.setText("La prova contiene troppi elementi per visuualizzarli tutti contemporaneamente: queste sono le ultime 1000 risoluzioni:\n");
			for(int i= prova.size()-1000; i < prova.size();i++){
				String s = prova.get(i);
				java.awt.EventQueue.invokeLater(() -> outputArea.append(s));
			}
		}
		this.tempoProva = tempoProva;
		generazioneRisultatoCompletata(prova.size());
		sbloccaPulsanti();
	}
	
	private void bloccaPulsanti(){
		inputButton.setEnabled(false);
		sussunzioneCheckBox.setEnabled(false);
		vsidsRadioButton.setEnabled(false);
		greedyRadioButton.setEnabled(false);
		bothRadioButton.setEnabled(false);
		provaCheckBox.setEnabled(false);
		scfRadioButton.setEnabled(false);
	}
	
	private void sbloccaPulsanti(){
		inputButton.setEnabled(true);
		sussunzioneCheckBox.setEnabled(true);
		vsidsRadioButton.setEnabled(true);
		greedyRadioButton.setEnabled(true);
		bothRadioButton.setEnabled(true);
		scfRadioButton.setEnabled(true);
	}
	
	public boolean generaProva(){
		return provaCheckBox.isSelected();
	}
    
    public void inizioLettura(){
		infoLabel.setText("Lettura da file in corso ...");
		infoLabel2.setText("");
    }
    
    public void fineLettura(){
		infoLabel.setText("La lettura del file \u00E8 stata completata, \u00E8 possibile eseguire la computazione"); 
		infoLabel2.setText("");
		computazione_completata = false;
		risultato_ricevuto = false;
		processoRisultato = "";
		tempoComputazione = 0;
		tempoProva = 0;
		provaCheckBox.setEnabled(true);
		jProgressBar1.setValue(0);
		jProgressBar1.setVisible(false);
		progresso = 0;
		outputArea.setText("");
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        inputButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        outputArea = new javax.swing.JTextArea();
        startButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        infoLabel = new javax.swing.JLabel();
        provaCheckBox = new javax.swing.JCheckBox();
        sussunzioneCheckBox = new javax.swing.JCheckBox();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        fileNameLabel = new javax.swing.JLabel();
        vsidsRadioButton = new javax.swing.JRadioButton();
        greedyRadioButton = new javax.swing.JRadioButton();
        bothRadioButton = new javax.swing.JRadioButton();
        infoLabel2 = new javax.swing.JLabel();
        scfRadioButton = new javax.swing.JRadioButton();
        jProgressBar1 = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        inputButton.setText("Seleziona un file in input");
        inputButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputButtonActionPerformed();
            }
        });

        outputArea.setEditable(false);
        outputArea.setColumns(20);
        outputArea.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        outputArea.setRows(5);
        outputArea.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane1.setViewportView(outputArea);

        startButton.setText("Avvia la risoluzione");
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed();
            }
        });

        jLabel1.setText("Risolutore SAT con la tecnica CDCL");

        infoLabel.setText("Info1");

        provaCheckBox.setText("Mostra la prova in output (se insoddisfacibile)");
        provaCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                provaCheckBoxActionPerformed();
            }
        });

        sussunzioneCheckBox.setText("Utilizzare la sussunzione per ottimizzazione");
        sussunzioneCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sussunzioneCheckBoxActionPerformed();
            }
        });

        jRadioButton1.setText("Utilizza la visualizzazione ad albero");

        jRadioButton2.setText("Utilizza la visualizzazione per righe");

        vsidsRadioButton.setText("Utilizza l'euristica VSIDS");
        vsidsRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vsidsRadioButtonActionPerformed();
            }
        });

        greedyRadioButton.setText("Utilizza l'euristica Greedy");
        greedyRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                greedyRadioButtonActionPerformed();
            }
        });

        bothRadioButton.setText("Utilizza le euristiche VSIDS, Greedy e SCF in parallelo con la condivisione delle informazioni");
        bothRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bothRadioButtonActionPerformed();
            }
        });

        infoLabel2.setText("Info2");

        scfRadioButton.setText("Utilizza l'euristics SCF (ottimale solo per il problema pigeonhole)");
        scfRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                scfRadioButtonActionPerformed();
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(vsidsRadioButton)
                            .addComponent(sussunzioneCheckBox)
                            .addComponent(provaCheckBox)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jRadioButton2)
                                    .addComponent(jRadioButton1)))
                            .addComponent(greedyRadioButton)
                            .addComponent(bothRadioButton)
                            .addComponent(scfRadioButton)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(172, 172, 172)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(infoLabel2)
                            .addComponent(infoLabel)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(158, 158, 158)
                        .addComponent(jLabel1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(inputButton)
                        .addGap(47, 47, 47)
                        .addComponent(fileNameLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(startButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(inputButton)
                    .addComponent(fileNameLabel)
                    .addComponent(startButton))
                .addGap(26, 26, 26)
                .addComponent(infoLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(infoLabel2)
                .addGap(19, 19, 19)
                .addComponent(vsidsRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(greedyRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scfRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bothRadioButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(provaCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jRadioButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jRadioButton2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sussunzioneCheckBox)
                .addGap(21, 21, 21)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void inputButtonActionPerformed() {//GEN-FIRST:event_inputButtonActionPerformed
        startButton.setEnabled(false);
        chooser.setVisible(true);        
    }//GEN-LAST:event_inputButtonActionPerformed

    public boolean sussunzioneAttivata() {
		return sussunzioneCheckBox.isSelected();
	}
	
	public void aggiornaProgresso(int progress, int max){
		jProgressBar1.setMaximum(max);
		if(progresso < progress){
			progresso = progress;
			jProgressBar1.setValue(progress);
		}
	}
    
    private void startButtonActionPerformed() {//GEN-FIRST:event_startButtonActionPerformed
		computazione_completata = false;
		risultato_ricevuto = false;
		java.awt.EventQueue.invokeLater(() -> infoLabel.setText("Computazione in corso ..."));
		jProgressBar1.setVisible(true);
		jProgressBar1.setMaximum(1);
		jProgressBar1.setValue(0);
		infoLabel.repaint();
		bloccaPulsanti();
        outputArea.setText("");
        startButton.setEnabled(false); 
		if(vsidsRadioButton.isSelected() || bothRadioButton.isSelected()){
			t1 = new Thread(() -> core1.compute(Core.EURISTICA_VSIDS, "t1", bothRadioButton.isSelected()));
			t1.start();
		}
		if(greedyRadioButton.isSelected() || bothRadioButton.isSelected()){
			t2 = new Thread(() -> core2.compute(Core.EURISTICA_GREEDY, "t2", bothRadioButton.isSelected()));
			t2.start();
		}
		if(scfRadioButton.isSelected() || bothRadioButton.isSelected()){
			t3 = new Thread(() -> core3.compute(Core.EURISTICA_SCF, "t3", bothRadioButton.isSelected()));
			t3.start();
		}
    }//GEN-LAST:event_startButtonActionPerformed

    private void provaCheckBoxActionPerformed() {//GEN-FIRST:event_provaCheckBoxActionPerformed
        if(computazione_completata && provaCheckBox.isSelected()){
			bloccaPulsanti();
			java.awt.EventQueue.invokeLater(() -> infoLabel2.setText("Generazione della "+(risultato ? "traccia" : "prova")+" in corso"));
			switch (processoRisultato) {
				case "t1":
					t1 = new Thread(core1::generaProvaEdInvialaAllInterfaccia);
					t1.start();
					break;
				case "t2":
					t2 = new Thread(core2::generaProvaEdInvialaAllInterfaccia);
					t2.start();
					break;
				default:
					t3 = new Thread(core3::generaProvaEdInvialaAllInterfaccia);
					t3.start();
					break;
			}
        }
        if(!provaCheckBox.isSelected()){
			outputArea.setText("");
			jRadioButton1.setVisible(false);
			jRadioButton2.setVisible(false);
			try{
				switch (processoRisultato) {
					case "t1":
						core1.interrompiGenerazioneProva();
						break;
					case "t2":
						core2.interrompiGenerazioneProva();
						break;
					default:
						core3.interrompiGenerazioneProva();
						break;
				}
			}
			catch(Exception e){}
        }
    }//GEN-LAST:event_provaCheckBoxActionPerformed

    private void sussunzioneCheckBoxActionPerformed() {//GEN-FIRST:event_sussunzioneCheckBoxActionPerformed
		if(computazione_completata)
			startButton.setEnabled(true);
    }//GEN-LAST:event_sussunzioneCheckBoxActionPerformed

	private void vsidsRadioButtonActionPerformed() {//GEN-FIRST:event_vsidsRadioButtonActionPerformed
        startButton.setEnabled(true);
    }//GEN-LAST:event_vsidsRadioButtonActionPerformed

    private void greedyRadioButtonActionPerformed() {//GEN-FIRST:event_greedyRadioButtonActionPerformed
        startButton.setEnabled(true);
    }//GEN-LAST:event_greedyRadioButtonActionPerformed

    private void bothRadioButtonActionPerformed() {//GEN-FIRST:event_bothRadioButtonActionPerformed
        startButton.setEnabled(true);
    }//GEN-LAST:event_bothRadioButtonActionPerformed

    private void scfRadioButtonActionPerformed() {//GEN-FIRST:event_scfRadioButtonActionPerformed
        startButton.setEnabled(true);
    }//GEN-LAST:event_scfRadioButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | javax.swing.UnsupportedLookAndFeelException | IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		//</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> new GUI().setVisible(true));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton bothRadioButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JLabel fileNameLabel;
    private javax.swing.JRadioButton greedyRadioButton;
    private javax.swing.JLabel infoLabel;
    private javax.swing.JLabel infoLabel2;
    private javax.swing.JButton inputButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea outputArea;
    private javax.swing.JCheckBox provaCheckBox;
    private javax.swing.JRadioButton scfRadioButton;
    private javax.swing.JButton startButton;
    private javax.swing.JCheckBox sussunzioneCheckBox;
    private javax.swing.JRadioButton vsidsRadioButton;
    // End of variables declaration//GEN-END:variables
}
