import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Viktor
 */
public class Clausola implements Comparable{
    private int numero_clausola;
    ArrayList<Integer> letterali;
	HashSet<Integer> hashLetterali;
    
    public int getNumeroClausola(){
        return numero_clausola;
    }
	
    public Clausola(int num){
        numero_clausola = num;
        letterali = new ArrayList<>();
		hashLetterali = new HashSet<>();
    }
    
    public Clausola(){
        letterali = new ArrayList<>();
		hashLetterali = new HashSet<>();
    }
    
    public void aggiungiLetterale(Letterale l){
        aggiungiLetterale(l.getValore());
    }
	
	public void aggiungiLetterale(Integer l){
        letterali.add(l);
		hashLetterali.add(l);
    }
    
    public boolean presente(int valore){
		return hashLetterali.contains(valore);
    }
    
    public boolean presenteNegato(Letterale l){
        return presente(-(l.getValore()));
    }
	
	public boolean presenteNegato(int l){
        return presente(-l);
    }
    
    public boolean contiene(Clausola c) {
        return (letterali.size() >= c.letterali.size()) && hashLetterali.containsAll(c.hashLetterali);
    }
	
	public boolean contiene(int l){
		return hashLetterali.contains(l);
    }
    
	@Override
	public int hashCode(){
		int hash = 151;
		for(Integer l: letterali){
			hash+=l.hashCode();
		}
		return hash;
	}
	
	@Override
	public boolean equals(Object c) {
		if (this == c) {
		    return true;
		}
		
		if (c instanceof Clausola) {
			if(letterali.size() == ((Clausola)c).letterali.size()){
				return this.contiene((Clausola)c);
			}
			else
				return false;
		}
		return false;
	}
    
    public boolean clausolaUnitaria(){
        return letterali.size() == 1;
    }
    
    @Override
    public String toString(){
        StringBuilder s = new StringBuilder();
        if(letterali.size() > 0){
            for(Integer l: letterali){
                s.append(l.toString());
				s.append(" ");
            }
        }
        else
            s.append("\u25A1");
        return s.toString();
    }
	
	public int compareTo(Clausola c){
		if(this.letterali.size() > c.letterali.size())
			return 1;
		else if(this.letterali.size() < c.letterali.size())
			return -1;
		else 
			return 0;
	}
    
    public boolean haGenitori(){
        return false;
    }
    
    public void setNumero(int num){
        numero_clausola = num;
    }

	@Override
	public int compareTo(Object o) {
		if(o instanceof Clausola){
			if(this.letterali.size() > ((Clausola) o).letterali.size())
				return 1;
			else if(this.letterali.size() < ((Clausola) o).letterali.size())
				return -1;
			else 
				return 0;
		}
		else{
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}
	}
    
}
