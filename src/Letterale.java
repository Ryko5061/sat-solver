 
/**
 *
 * @author Viktor
 */
public class Letterale implements Comparable{
    private final int valore;
    
    public Letterale(int letterale) {
        valore = letterale;
    }
    
    public int getValoreAssoluto() {
		if (valore < 0)
			return -valore;
		else
			return valore;
	}
    
    public int getValore(){
        return valore;
    }
    
	@Override
    public String toString(){
		return valore+"";
    }
    
	@Override
	public int hashCode(){
		return valore;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Letterale other = (Letterale) obj;
		return this.valore == other.valore;
	}

	@Override
	public int compareTo(Object o) {
		if(o instanceof Letterale){
			if(valore > ((Letterale)o).valore)
				return 1;
			if(valore < ((Letterale)o).valore)
				return -1;
			return 0;
		}
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}
