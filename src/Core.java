import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import static java.lang.System.exit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Core {
	private int numVar;
	public static final int EURISTICA_VSIDS = 1;
	public static final int EURISTICA_GREEDY = 2;
	public static final int EURISTICA_SCF = 3;
    private int numClausole;
    private StrutturaDati data;
    private final GUI inter;
    private int livello_corrente = 0;
    private boolean finito = false;
    private int variabile_selezionata;
    private final LinkedList<LetteraleSullaTraccia> traccia;
    private boolean soddisfatto = false; 
    private ClausolaRisolvente ultimaClausolaAppresa;
    private LinkedList<ClausolaRisolvente> listaRis = new LinkedList<>();
	private double start;
	private double finish;
	private double tempoProva;
	private String nomeProcesso;
	private int assegnamentoScore = 1;
	private int contatorePerVSIDS = 0;
	private boolean vsids;
	private int euristica;
	private boolean interruzioneEsterna = false;
	private final int MAX_CONTATORE_VSIDS = 65000;
	private String tracciaRisultato = "";
	private int contatoreRestart = 0;
	private boolean ciSonoAltreThread = false;
	private final LinkedList<LetteraleSullaTraccia> daPropagare = new LinkedList<>();
	private final HashSet<Integer> daPropagareHash = new HashSet<>();
	private final LinkedList<String> prova = new LinkedList<>();
	private boolean stopGenerazioneProva = false;
	private StringBuilder s;
	private int[] sequenzaRestart = {100, 100, 200, 100, 100, 200, 400};
	private int posSequenzaRestart = 0;
    private int numConflitti = 0;
    private int contPropagazioni = 0;
    private int numRestart=0;
	private int tracciaUltimaSemplificazione = 0;
	private int clausoleAppreseEliminate = 0;
	private Clausola clausolaInPropagazione;
	private int dimClausoleCondivisePrima = 0;
	private float fattoreOverflow = 2;
	private boolean cicloEliminazione = true;
	private Iterator iter;
	private int numClausolaInPropagazione;
	private LetteraleSullaTraccia let;
	private Map<Integer, ArrayList<Integer>> mappaVariabiliNegate;
	private Map<Integer, Clausola> mappaClausole;
	private Map<Integer, ClausolaRisolvente> mappaClausoleApprese;
	private boolean conflitto;
	private final LinkedHashSet<Integer> daEliminare = new LinkedHashSet<>();
	private boolean generaProva;
	private ArrayList<Integer> coppia;
	private int clausoleSussunte = 0;
	private int clausoleSemplificate = 0;
    
    public Core(GUI i){
        this.inter = i;
        traccia = new LinkedList<>();
    }
    
    public void compute(int euristica, String processo, boolean ci_sono_altre_thread){
		System.out.println("Inizio computazione thread "+processo);
		this.euristica = euristica;
		vsids = euristica == EURISTICA_VSIDS;
		ciSonoAltreThread = ci_sono_altre_thread;
		nomeProcesso = processo;
		generaProva = inter.generaProva();
		start = System.nanoTime();
        if(inter.sussunzioneAttivata())
            clausoleSussunte = data.sussunzione(); //elimino clausole contenute in altre clausole
		
		propagazioneIniziale();
        
        while(!finito && !interruzioneEsterna){
            if(verificaTutteLeClausoleSoddisfatte()){
                soddisfatto = true;
                finito = true;
            }
            else{
				if(livello_corrente == 0){
					if(traccia.size() > 0 && traccia.size() > tracciaUltimaSemplificazione){
						clausoleSemplificate += semplificazione();
					}
				}
				decisione();
            }
        }
		inter.aggiornaProgresso(1, 1);
        if(!interruzioneEsterna){
			finish = System.nanoTime();
			double tempoComputazione = finish - start;
			inter.riceviRisultato(soddisfatto, tempoComputazione, nomeProcesso);
			inter.computazioneCompletata();
			
			if(soddisfatto){
				tracciaRisultato = getTraccia();
				finish = System.nanoTime();
				tempoProva = finish - start - tempoComputazione;
				inter.riceviTraccia(tracciaRisultato, tempoProva);
			}
			else{
				if(inter.mostraProvaAttivo()){
					getProva();
					if(!stopGenerazioneProva){
						finish = System.nanoTime();
						tempoProva = finish - start - tempoComputazione;
						try {
							if(!stopGenerazioneProva)
								inter.riceviProva(prova, tempoProva);
						}
						catch(ConcurrentModificationException e) {}
					}
				}
			}
			System.out.println("Numero conflitti trovati: "+numConflitti);
            System.out.println("Numero propagazioni: "+contPropagazioni);
            System.out.println("Numero restart: "+numRestart);
			System.out.println("Clausole apprese eliminate: "+clausoleAppreseEliminate);
			System.out.println("Clausole sussunte: "+clausoleSussunte);
			System.out.println("Clausole eliminate per semplificazione: "+clausoleSemplificate);
		}
	}
	
	private int semplificazione(){
		//System.out.println("Semplificazione "+nomeProcesso);
		int cont = 0;
		LetteraleSullaTraccia l;
		for(int i = tracciaUltimaSemplificazione - 1 >  -1 ? tracciaUltimaSemplificazione -1 : 0 ; i < traccia.size();i++){
			l = traccia.get(i);
			if(l.getValore() > 0){
				for(int clausola: data.mappaVariabiliNonNegate().get(l.getValore())){
					daEliminare.add(clausola);
					cont++;
				}
			}
			else{
				for(int clausola: data.mappaVariabiliNegate().get(-l.getValore())){
					daEliminare.add(clausola);
					cont++;
				}
			}
		}
		for(int i: daEliminare){
			data.eliminaClausola(data.mappaClausole().get(i));
		}
		daEliminare.clear();
		tracciaUltimaSemplificazione = traccia.size();
		return cont;
	}
	
	private void propagazioneIniziale(){
		for(Clausola cla: data.mappaClausole().values()){
			if(cla.letterali.size() == 1){
				int valore = cla.letterali.get(0);
				aggiungiDaPropagare(new LetteraleSullaTraccia(valore, LetteraleSullaTraccia.PROPAGATO, 0, cla));
				if(valore > 0){
					data.getWatchedClausoleNonNegate().get(valore).add(cla.getNumeroClausola());
				}
				else{
					data.getWatchedClausoleNegate().get(-valore).add(cla.getNumeroClausola());
				}
			}
			else{
				int valore = cla.letterali.get(0);
				if(valore > 0){
					data.getWatchedClausoleNonNegate().get(valore).add(cla.getNumeroClausola());
				}
				else{
					data.getWatchedClausoleNegate().get(-valore).add(cla.getNumeroClausola());
				}
				valore = cla.letterali.get(1);
				if(valore > 0){
					data.getWatchedClausoleNonNegate().get(valore).add(cla.getNumeroClausola());
				}
				else{
					data.getWatchedClausoleNegate().get(-valore).add(cla.getNumeroClausola());
				}
			}
		}
		propagazioneSuTuttoConAggiuntaSullaTraccia();
	}
	
	public void generaProvaEdInvialaAllInterfaccia(){
		if(!soddisfatto){
			start = System.nanoTime();
			getProva();
			finish = System.nanoTime();
			tempoProva = finish - start;
			inter.riceviProva(prova, tempoProva);
		}
		else{
			start = System.nanoTime();
			tracciaRisultato = getTraccia();
			finish = System.nanoTime();
			tempoProva = finish - start;
			inter.riceviTraccia(tracciaRisultato, tempoProva);
		}
	}
    
    private String getTraccia(){
		s = new StringBuilder();
        for(LetteraleSullaTraccia l:traccia){
			s.append(l.getValore());
			s.append(" ");
            if(l.tipo == LetteraleSullaTraccia.DECISO){
				s.append("deciso\n");
			}
            else{
				s.append("implicato\n");
            }
        }
        return s.toString();
    }
    
    private void getProva(){
		getAlbero(ultimaClausolaAppresa, 0);
    }
	
	private void getAlbero(Clausola c, int livello){
		if(livello == 12)
			return;
		if(stopGenerazioneProva)
			return;
		if(c instanceof ClausolaRisolvente){
			if(((ClausolaRisolvente) c).padre1 != null)
				if(((ClausolaRisolvente) c).padre1.haGenitori())
					getAlbero(((ClausolaRisolvente) c).padre1, livello+1);
			if(((ClausolaRisolvente) c).padre2 != null)
				if(((ClausolaRisolvente) c).padre2.haGenitori())
					getAlbero(((ClausolaRisolvente) c).padre2, livello+1);
			if(c.haGenitori()){
				s = new StringBuilder();
				s.append(((ClausolaRisolvente) c).padre1.toString());
				s.append(" AND ");
				s.append(((ClausolaRisolvente) c).padre2.toString());
				s.append(" --> ");
				s.append(c.toString());
				s.append("\n");
				prova.add(s.toString());
			}
		}
		else{
			prova.add(c.toString());
		}
	}
    
	//la verifica è solo sulle clausole iniziali
    private boolean verificaTutteLeClausoleSoddisfatte(){
		return traccia.size() == data.veroNumeroVar();
    }
	
	public void interrompi(){
		finito = true;
		interruzioneEsterna = true;
	}
    
    private void decisione(){
		if(contatoreRestart >= sequenzaRestart[posSequenzaRestart]){
			contatoreRestart = 0;
			if(posSequenzaRestart == sequenzaRestart.length - 1){
				posSequenzaRestart = 0;
				for(int i=0;i<sequenzaRestart.length;i++){
					sequenzaRestart[i] = sequenzaRestart[i]*2;
				}
			}
			else{
				posSequenzaRestart++;
			}
			restart();
		}

		if(ciSonoAltreThread){
			if(data.clausoleNuoveSize() > dimClausoleCondivisePrima){
				dimClausoleCondivisePrima = data.sincronizzaClausoleNuove(nomeProcesso);
				if(data.clausoleNuoveSize() > 0){
					saltaAlLivelloZeroSenzaAggiungerePropagazioni();
					controllaClausoleNuove();
					if(daPropagare.size() > 0){
						propagazioneSuTuttoConAggiuntaSullaTraccia();
					}
				}
			}
		}
		
		if(livello_corrente == 0){
			inter.aggiornaProgresso(traccia.size(), numVar);
		}
		
		if(!finito){
			livello_corrente++;
			variabile_selezionata = data.getNextAssegnamentoVariabile(euristica);
			if(variabile_selezionata == 0)
				return;
			//System.out.println("DECISIONE "+variabile_selezionata+" al livello "+livello_corrente);
			aggiungiDaPropagare(new LetteraleSullaTraccia(variabile_selezionata,LetteraleSullaTraccia.DECISO, livello_corrente));
			propagazioneSuTuttoConAggiuntaSullaTraccia();
		
			if(vsids){
				assegnamentoScore=assegnamentoScore+10;
				contatorePerVSIDS++;
				if(contatorePerVSIDS == MAX_CONTATORE_VSIDS){
					contatorePerVSIDS = 0;
					data.dimezzaTuttiGliScore();
				}
			}
		}
    }
	
	private void aggiungiClausolaAppresaWatched(int let, int numeroClausola){
		if(let > 0){
			data.getWatchedClausoleAppreseNonNegate().get(let).add(numeroClausola);
		}
		else{
			data.getWatchedClausoleAppreseNegate().get(-let).add(numeroClausola);
		}
	}
	
	private void aggiungiClausolaWatched(int let, int numeroClausola){
		if(let > 0){
			data.getWatchedClausoleNonNegate().get(let).add(numeroClausola);
		}
		else{
			data.getWatchedClausoleNegate().get(-let).add(numeroClausola);
		}
	}
	
	private void controllaClausoleNuove(){
		for(int clausola: data.clausoleNuove()){
			sistemaClausolaALivelloZero(data.mappaClausoleApprese().get(clausola));
		}
		data.clausoleNuove().clear();
	}
	
	private void swapWatchedClausolaAppresa(Clausola c, int indice1, int indice2){
		Collections.swap(c.letterali, indice1, indice2);
		if(indice2 > 1){
			int let = c.letterali.get(indice1);
			if(let > 0){
				data.getWatchedClausoleAppreseNonNegate().get(let).add(c.getNumeroClausola());
			}
			else{
				data.getWatchedClausoleAppreseNegate().get(-let).add(c.getNumeroClausola());
			}
			let = c.letterali.get(indice2);
			if(let > 0){
				data.getWatchedClausoleAppreseNonNegate().get(let).remove((Integer)c.getNumeroClausola());
			}
			else{
				data.getWatchedClausoleAppreseNegate().get(-let).remove((Integer)c.getNumeroClausola());
			}
		}
	}
	
	private void sistemaClausolaALivelloZero(Clausola clausolaInPropagazione){
		if(clausolaInPropagazione != null){
			if(data.clausolaSoddisfatta(clausolaInPropagazione)){
				spostaIlletteraleSoddisfattoAlPrimoPosto(clausolaInPropagazione);
				int primoLet = clausolaInPropagazione.letterali.get(0);
				if(primoLet > 0){
					data.getWatchedClausoleAppreseNonNegate().get(primoLet).add(clausolaInPropagazione.getNumeroClausola());
				}
				else{
					data.getWatchedClausoleAppreseNegate().get(-primoLet).add(clausolaInPropagazione.getNumeroClausola());
				}
				if(clausolaInPropagazione.letterali.size() > 1){
					int secondoLet = clausolaInPropagazione.letterali.get(1);
					if(secondoLet > 0){
						data.getWatchedClausoleAppreseNonNegate().get(secondoLet).add(clausolaInPropagazione.getNumeroClausola());
					}
					else{
						data.getWatchedClausoleAppreseNegate().get(-secondoLet).add(clausolaInPropagazione.getNumeroClausola());
					}
				}
				return;
			}
			//clausola unitaria non soddisfatta
			if(clausolaInPropagazione.letterali.size() == 1){
				Variabile var = data.variabili()[modulo(clausolaInPropagazione.letterali.get(0))];
				boolean primoOk = var.getAssegnamento() == Variabile.NON_ASSEGNATO;
				if(!primoOk){
					if(generaProva)
						conflittoALivelloZero(clausolaInPropagazione);
					finito = true;
					soddisfatto = false;
				}
				else{
					aggiungiDaPropagare(new LetteraleSullaTraccia(clausolaInPropagazione.letterali.get(0),livello_corrente, clausolaInPropagazione));
				}
			}
			else{
				//controllo primo watchhed literals
				boolean primoOk = data.variabileNonAssegnata(modulo(clausolaInPropagazione.letterali.get(0)));
				boolean secondoOk = data.variabileNonAssegnata(modulo(clausolaInPropagazione.letterali.get(1)));
				//cerca un nuovo watched literal
				//entrambi i watched sono undefined
				if(primoOk && secondoOk)
					return;
				if(clausolaInPropagazione.letterali.size() > 2){
					for(int pos2 = 2; pos2 < clausolaInPropagazione.letterali.size(); pos2++){
						//se lo trovo
						if(data.variabileNonAssegnata(modulo(clausolaInPropagazione.letterali.get(pos2)))){
							//eseguo lo swap per avere il primo o il secondo letterale watched giusto
							if(!primoOk){
								swapWatchedClausolaAppresa(clausolaInPropagazione, 0, pos2);
								//clausolaInPropagazione.watchedLiterals[0] = clausolaInPropagazione.letterali.get(pos2);
								primoOk = true;
								break;
							}
							else{
								swapWatchedClausolaAppresa(clausolaInPropagazione, 1, pos2);
								//clausolaInPropagazione.watchedLiterals[1] = clausolaInPropagazione.letterali.get(pos2);
								secondoOk = true;
								break;
							}
						}
					}
					if(!primoOk && !secondoOk){
						//conflitto
						if(generaProva)
							conflittoALivelloZero(clausolaInPropagazione);
						finito = true;
						soddisfatto = false;
					}
					else{
						//propagazione
						if(primoOk){
							aggiungiDaPropagare(new LetteraleSullaTraccia(clausolaInPropagazione.letterali.get(0),livello_corrente, clausolaInPropagazione));
						}
						else{
							aggiungiDaPropagare(new LetteraleSullaTraccia(clausolaInPropagazione.letterali.get(1), livello_corrente, clausolaInPropagazione));
						}
					}
				}
				else{
					//conflitto
					if(!primoOk && !secondoOk){
						//printTrace();
						if(generaProva)
							conflittoALivelloZero(clausolaInPropagazione);
						finito = true;
						soddisfatto = false;
					}		
					//propagazione
					else{
						if(primoOk){
							aggiungiDaPropagare(new LetteraleSullaTraccia(clausolaInPropagazione.letterali.get(0),livello_corrente, clausolaInPropagazione));
						}
						else{
							aggiungiDaPropagare(new LetteraleSullaTraccia(clausolaInPropagazione.letterali.get(1), livello_corrente, clausolaInPropagazione));
						}
					}
				}	
			}
		}
	}
	    
	private void aggiungiSullaTraccia(LetteraleSullaTraccia l){
		//System.out.println("Aggiunta alla traccia del letterale "+l.getValore()+(l.tipo == LetteraleSullaTraccia.PROPAGATO ? " con giustificazione "+l.getGiustificazione().toString() : ""));
		traccia.add(l);
		data.variabili()[l.getValoreAssoluto()].assegna(l.getValore() > 0 ? Variabile.UNO : Variabile.ZERO);
	}
	
	private void propagazione(int variabile) {
		if (variabile > 0) {
			mappaVariabiliNegate = data.getWatchedClausoleNegate();
		} else {
			mappaVariabiliNegate = data.getWatchedClausoleNonNegate();
		}

		mappaClausole = data.mappaClausole();
		conflitto = false;
		int modulo_variabile = modulo(variabile);
		int posConflitto = 0;
		coppia = (ArrayList<Integer>) mappaVariabiliNegate.get(modulo_variabile).clone();
		mappaVariabiliNegate.get(modulo_variabile).clear();

		for (int i = 0; i < coppia.size(); i++) {
			numClausolaInPropagazione = coppia.get(i);
			clausolaInPropagazione = mappaClausole.get(numClausolaInPropagazione);

			if (clausolaInPropagazione != null) {
				//clausola unitaria non soddisfatta
				if (clausolaInPropagazione.letterali.size() == 1) {
					if (livello_corrente == 0) {
						if (generaProva) {
							conflittoALivelloZero(clausolaInPropagazione);
						}
						conflitto = true;
						posConflitto = i;
						finito = true;
						soddisfatto = false;
						break;
					} else {
						conflitto(clausolaInPropagazione);
						conflitto = true;
						posConflitto = i;
						break; //per uscire dal for
					}
				} else {
					boolean primoOk = data.variabileOk(clausolaInPropagazione.letterali.get(0));
					boolean secondoOk = data.variabileOk(clausolaInPropagazione.letterali.get(1));
					//se uno dei due watched è assegnato vero salto tutto il resto della propagazione
					if (primoOk) {
						//VF
						if (!data.variabileNonAssegnata(modulo(clausolaInPropagazione.letterali.get(0)))) {
							secondoOk = true;
						}
					} 
					else if (secondoOk) {
						//FV
						if (!data.variabileNonAssegnata(modulo(clausolaInPropagazione.letterali.get(1)))) {
							primoOk = true;
						}
					}
					//VF/FV
					if(primoOk && secondoOk){
						aggiungiClausolaWatched(-variabile, clausolaInPropagazione.getNumeroClausola());
					}
					//FF
					else if (!primoOk && !secondoOk) {
						if (livello_corrente == 0) {
							//printTrace();
							if (generaProva) {
								conflittoALivelloZero(clausolaInPropagazione);
							}
							conflitto = true;
							finito = true;
							posConflitto = i;
							soddisfatto = false;
							break;
						} else {
							//printTrace();
							conflitto(clausolaInPropagazione);
							conflitto = true;
							posConflitto = i;
							break; //per uscire dal for
						}
					} //UF/FU
					//cerca un nuovo watched literal						
					else {
						if (clausolaInPropagazione.letterali.size() > 2) {
							for (int pos2 = 2; pos2 < clausolaInPropagazione.letterali.size(); pos2++) {
								//se lo trovo
								if (data.variabileOk(clausolaInPropagazione.letterali.get(pos2))) {
									//eseguo lo swap per avere il primo o il secondo letterale watched giusto
									if (!primoOk) {
										Collections.swap(clausolaInPropagazione.letterali, 0, pos2);
										//i -= swapWatchedClausola(clausolaInPropagazione, 0, pos2, PROPAGAZIONE_CLAUSOLE_INIZIALI);
										aggiungiClausolaWatched(clausolaInPropagazione.letterali.get(0), clausolaInPropagazione.getNumeroClausola());
										primoOk = true;
									} else {
										Collections.swap(clausolaInPropagazione.letterali, 1, pos2);
										//i -= swapWatchedClausola(clausolaInPropagazione, 1, pos2, PROPAGAZIONE_CLAUSOLE_INIZIALI);
										aggiungiClausolaWatched(clausolaInPropagazione.letterali.get(1), clausolaInPropagazione.getNumeroClausola());
										secondoOk = true;
									}
									break;
								}
							}
						}
						//UU
						if(primoOk && secondoOk){
							//ho già aggiunto la cluausla nella mappa del watched nuovo
						}
						//UF
						else if (!secondoOk) {
							aggiungiDaPropagare(new LetteraleSullaTraccia(clausolaInPropagazione.letterali.get(0), livello_corrente, clausolaInPropagazione));
							aggiungiClausolaWatched(-variabile, clausolaInPropagazione.getNumeroClausola());
						} //FU
						else  {
							aggiungiDaPropagare(new LetteraleSullaTraccia(clausolaInPropagazione.letterali.get(1), livello_corrente, clausolaInPropagazione));
							aggiungiClausolaWatched(-variabile, clausolaInPropagazione.getNumeroClausola());
						}
					}
				}
			}
		}
		if(conflitto){
			mappaVariabiliNegate.get(modulo_variabile).addAll(coppia.subList(posConflitto, coppia.size()));
		}
	}
	
	private boolean propagazioneClausoleApprese(int variabile){
		if(variabile > 0){
			mappaVariabiliNegate = data.getWatchedClausoleAppreseNegate();
		} else {
			mappaVariabiliNegate = data.getWatchedClausoleAppreseNonNegate();
		}

		mappaClausoleApprese = data.mappaClausoleApprese();
		conflitto = false;

		int modulo_variabile = modulo(variabile);
		int posConflitto = 0;
		
		coppia = (ArrayList<Integer>) mappaVariabiliNegate.get(modulo_variabile).clone();
		mappaVariabiliNegate.get(modulo_variabile).clear();

		for (int i = 0; i < coppia.size(); i++) {
			numClausolaInPropagazione = coppia.get(i);
			clausolaInPropagazione = mappaClausoleApprese.get(numClausolaInPropagazione);

			if (clausolaInPropagazione != null) {
				//clausola unitaria non soddisfatta
				if (clausolaInPropagazione.letterali.size() == 1) {
					if (livello_corrente == 0) {
						if (generaProva) {
							conflittoALivelloZero(clausolaInPropagazione);
						}
						conflitto = true;
						posConflitto = i;
						finito = true;
						soddisfatto = false;
						break;
					} else {
						conflitto(clausolaInPropagazione);
						conflitto = true;
						posConflitto = i;
						break; //per uscire dal for
					}
				} else {
					boolean primoOk = data.variabileOk(clausolaInPropagazione.letterali.get(0));
					boolean secondoOk = data.variabileOk(clausolaInPropagazione.letterali.get(1));
					//se uno dei due watched è assegnato vero salto tutto il resto della propagazione
					if (primoOk) {
						//VF
						if (!data.variabileNonAssegnata(modulo(clausolaInPropagazione.letterali.get(0)))) {
							secondoOk = true;
						}
					} 
					else if (secondoOk) {
						//FV
						if (!data.variabileNonAssegnata(modulo(clausolaInPropagazione.letterali.get(1)))) {
							primoOk = true;
						}
					}
					//VF/FV
					if(primoOk && secondoOk){
						aggiungiClausolaAppresaWatched(-variabile, clausolaInPropagazione.getNumeroClausola());
					}
					//FF
					else if (!primoOk && !secondoOk) {
						if (livello_corrente == 0) {
							//printTrace();
							if (generaProva) {
								conflittoALivelloZero(clausolaInPropagazione);
							}
							conflitto = true;
							finito = true;
							posConflitto = i;
							soddisfatto = false;
							break;
						} else {
							//printTrace();
							conflitto(clausolaInPropagazione);
							conflitto = true;
							posConflitto = i;
							break; //per uscire dal for
						}
					} //UF/FU
					//cerca un nuovo watched literal						
					else {
						if (clausolaInPropagazione.letterali.size() > 2) {
							for (int pos2 = 2; pos2 < clausolaInPropagazione.letterali.size(); pos2++) {
								//se lo trovo
								if (data.variabileOk(clausolaInPropagazione.letterali.get(pos2))) {
									//eseguo lo swap per avere il primo o il secondo letterale watched giusto
									if (!primoOk) {
										Collections.swap(clausolaInPropagazione.letterali, 0, pos2);
										//i -= swapWatchedClausola(clausolaInPropagazione, 0, pos2, PROPAGAZIONE_CLAUSOLE_INIZIALI);
										aggiungiClausolaAppresaWatched(clausolaInPropagazione.letterali.get(0), clausolaInPropagazione.getNumeroClausola());
										primoOk = true;
									} else {
										Collections.swap(clausolaInPropagazione.letterali, 1, pos2);
										//i -= swapWatchedClausola(clausolaInPropagazione, 1, pos2, PROPAGAZIONE_CLAUSOLE_INIZIALI);
										aggiungiClausolaAppresaWatched(clausolaInPropagazione.letterali.get(1), clausolaInPropagazione.getNumeroClausola());
										secondoOk = true;
									}
									break;
								}
							}
						}
						//UU
						if(primoOk && secondoOk){
							//ho già aggiunto la cluaosla nella mappa del watched nuovo
						}
						//UF
						else if (!secondoOk) {
							aggiungiDaPropagare(new LetteraleSullaTraccia(clausolaInPropagazione.letterali.get(0), livello_corrente, clausolaInPropagazione));
							aggiungiClausolaAppresaWatched(-variabile, clausolaInPropagazione.getNumeroClausola());
						} //FU
						else  {
							aggiungiDaPropagare(new LetteraleSullaTraccia(clausolaInPropagazione.letterali.get(1), livello_corrente, clausolaInPropagazione));
							aggiungiClausolaAppresaWatched(-variabile, clausolaInPropagazione.getNumeroClausola());
						}
					}
				}
			}
		}
		if(conflitto){
			mappaVariabiliNegate.get(modulo_variabile).addAll(coppia.subList(posConflitto, coppia.size()));
		}
		return conflitto;
	}
	
	private void propagazioneSuTuttoConAggiuntaSullaTraccia(){
		if(interruzioneEsterna){
			return;
		}
		contPropagazioni++;
		
		while(!daPropagare.isEmpty()){
			
			LetteraleSullaTraccia l = daPropagare.getFirst();
			//System.out.println("Propagazione "+l.getValore());
			daPropagare.removeFirst();
			daPropagareHash.remove(l.getValore());
			int variabile = l.getValore();
			conflitto = false;
			if(!finito){
				//if(possoAggiungereSullaTraccia(l)){
				aggiungiSullaTraccia(l);
				if(!data.mappaClausoleApprese().isEmpty()){
					conflitto = propagazioneClausoleApprese(variabile);
				}
				//se ho avuto un conflitto avrò fatto anche il salto all'indietro
				if(!conflitto){
					propagazione(variabile);
				}			
			}
		}
    }
	
    private void disfaUltimoLetteraleSullaTracciaSenzaAggiungerePropagazioni(){
        let = traccia.getLast();
		//System.out.println("Disfo dalla traccia: "+let);
		data.variabili()[let.getValoreAssoluto()].annulla_assegnamento();

        //metto la relativa variabile come non assegnata
        traccia.removeLast();
        if(traccia.size() > 0){
            livello_corrente = traccia.getLast().getLivello();
        }
        else{
            livello_corrente = 0;
        }
    }
	
	private void disfaUltimoLivelloSullaTracciaSenzaAggiungerePropagazioni(){
		int livello = traccia.getLast().getLivello();
		while(livello_corrente == livello){
			disfaUltimoLetteraleSullaTracciaSenzaAggiungerePropagazioni();
			if(traccia.isEmpty())
				break;
		}  
    }
	
	private void saltaAlLivelloZeroSenzaAggiungerePropagazioni(){
		while(livello_corrente > 0){
			if(traccia.size() > 0){
				disfaUltimoLetteraleSullaTracciaSenzaAggiungerePropagazioni();
				if(livello_corrente == 0)
					break;
			}
			else 
				break;
		} 
	}
	
	private void spostaIlletteraleSoddisfattoAlPrimoPosto(Clausola cc){
		//System.out.println("Imposto soddisfatta la clausola num. "+cc.getNumeroClausola()+": "+cc.toString());
		for(int i = 0;i < cc.letterali.size();i++){
			if(data.variabileSoddisfa(cc.letterali.get(i))){
				swapWatchedClausolaAppresa(cc, 0, i);
				break;
			}
		}
    }
    
    private void conflitto(Clausola c1){
		
        //System.out.println("CONFLITTO (clausola "+c1.getNumeroClausola()+") "+c1.toString());
		numConflitti++;
		contatoreRestart++;
		aumentaScore(c1);
        int contatore;
        ClausolaRisolvente c = new ClausolaRisolvente(c1);
		
        //printTrace();
		boolean esci;
        do{
            //printTrace();
			//non devo risolvere rispetto a propagazioni che non hanno nulla a che fare con la clausola sulla quale sto risolvendo
			esci = false;
			while(!esci){
				if(!c.presenteNegato(traccia.getLast())){
					//per evitare risoluzioni SBAGLIATE
					disfaUltimoLetteraleSullaTracciaSenzaAggiungerePropagazioni();
				}
				else{
					esci = true;
				}
			}
						
            c = risoluzioneConSussunzione(c, traccia.getLast().getGiustificazione());
						
			//devo fermarmi quando solo un letterale è stato negato al livello corrente
                    
			disfaUltimoLetteraleSullaTracciaSenzaAggiungerePropagazioni(); //backjump implicito
						
			//controllo: c è di asserzione?
			contatore = 0;
			
			
			esci = false;
			while(!esci){
				if(!c.presenteNegato(traccia.getLast())){
					//per evitare risoluzioni SBAGLIATE
					disfaUltimoLetteraleSullaTracciaSenzaAggiungerePropagazioni();
				}
				else{
					esci = true;
				}
			}
			
                        
			Iterator iter2 = traccia.descendingIterator();
            while(iter2.hasNext()){
				LetteraleSullaTraccia ltraccia = (LetteraleSullaTraccia) iter2.next();
				if(ltraccia.getLivello() == livello_corrente){
					if(c.contiene(-ltraccia.getValore())){
                        contatore ++;
                    }
                    //sono arrivato al livello precedente		
                }
				else {
					break;
				}
				if(contatore > 1)
					break;
            }
			
        }while(contatore > 1);
		

        
		//elimino livelli di letterali dalla traccia finchè c non diventa soddisfacibile e poi propago sui watched literals
		if(c.letterali.size() >= 1){
			//di sicuro nel livello corrente è rimasta almeno la decisione, probabilmente anche delle ppropagazioni
			disfaUltimoLivelloSullaTracciaSenzaAggiungerePropagazioni();
			
			
			//System.out.println("La clausola è "+c+ " al livello "+livello_corrente);
			//faccio il salto più grande possibile
			
			if(!c.clausolaUnitaria()){
				while(nonCiSonoLetteraliNegatiUltimoLivello(c)){
					//System.out.println("Salto di più");
					disfaUltimoLivelloSullaTracciaSenzaAggiungerePropagazioni();
					if(livello_corrente == 0)
						break;
				}
			}
			else{
				saltaAlLivelloZeroSenzaAggiungerePropagazioni();
			}
		}
		//c clausola unitaria o clausola vuota
		else{		
			finito = true;
			soddisfatto = false;
			if(generaProva)
				conflittoALivelloZero(c);
		}
		//sistemazioneCodaPropagazioniDopoConflitto();
		sistemaPrimaDiApprendere(c);
		apprendimento(c);
    }
	
	private void sistemaPrimaDiApprendere(Clausola c){
		if(!data.variabileNonAssegnata(modulo(c.letterali.get(0)))){
			for(int i = 0; i< c.letterali.size();i++){
				if(data.variabileNonAssegnata(modulo(c.letterali.get(i)))){
					Collections.swap(c.letterali, 0, i);
					break;
				}
			}
		}
		iter = traccia.descendingIterator();
		while(iter.hasNext()){
			let = (LetteraleSullaTraccia) iter.next();
			if(c.contiene(-let.getValore())){
				for(int i = 1; i< c.letterali.size();i++){
					if(c.letterali.get(i) == -let.getValore()){
						if(i == 1){
							break;
						}
						Collections.swap(c.letterali, 1, i);
						break;
					}
				}
				break;
			}
		}
	}
	
	private boolean nonCiSonoLetteraliNegatiUltimoLivello(Clausola c){
		Iterator i = traccia.descendingIterator();
		while(i.hasNext()){
			LetteraleSullaTraccia l = (LetteraleSullaTraccia) i.next();
			if(l.getLivello() == livello_corrente){
				if(c.presente(-l.getValore()))
					return false;
			}
			else{
				break;
			}
		}
		return true;
	}
	
	private void conflittoALivelloZero(Clausola c1){
        //System.out.println("CONFLITTO A LIVELLO ZERO "+c1.toString());
        //itero sulla traccia a partiire dalla fine
		//printTrace();
		iter = traccia.descendingIterator();
		listaRis.add(new ClausolaRisolvente(c1));
        LetteraleSullaTraccia ltraccia;
        if(traccia.size() > 0){
            while(iter.hasNext()){
                ltraccia = (LetteraleSullaTraccia) iter.next();
				if(siPuoSussumere(listaRis.getLast(), ltraccia.getGiustificazione())){
					listaRis.add(risoluzioneConSussunzione(listaRis.getLast(), ltraccia.getGiustificazione()));
					if(listaRis.getLast().letterali.isEmpty())
						break;
				}
            }
        }   
        ultimaClausolaAppresa = listaRis.getLast();
		System.out.println("ultima clausola appresa: "+ultimaClausolaAppresa.toString());
    }
	
	private boolean siPuoSussumere(Clausola c1, Clausola c2){
		for(int l: c1.letterali){
			if(c2.presenteNegato(l))
				return true;
		}
		return false;
	}
	
	private void aggiungiDaPropagare(LetteraleSullaTraccia l){
		if(!daPropagareHash.contains(l.getValore()) && !daPropagareHash.contains(-l.getValore())){
			daPropagare.add(l);
			daPropagareHash.add(l.getValore());
		}
	}
    
    private void apprendimento(ClausolaRisolvente c){
		if(inter.sussunzioneAttivata()){
			//faccio la sussunzione solo se la clausola è unitaria
			if(c.clausolaUnitaria()){
				clausoleSussunte += data.sussunzione(c);
			}
		}
		//System.out.println("apprendo :"+c.toString());
		dimClausoleCondivisePrima = data.aggiungiClausola(c, nomeProcesso, ciSonoAltreThread, livello_corrente);
		ultimaClausolaAppresa = c;
		
		int primoLet = ultimaClausolaAppresa.letterali.get(0);
		if(primoLet > 0){
			data.getWatchedClausoleAppreseNonNegate().get(primoLet).add(ultimaClausolaAppresa.getNumeroClausola());
		}
		else{
			data.getWatchedClausoleAppreseNegate().get(-primoLet).add(ultimaClausolaAppresa.getNumeroClausola());
		}
		if(ultimaClausolaAppresa.letterali.size() > 1){
			int secondoLet = ultimaClausolaAppresa.letterali.get(1);
			if(secondoLet > 0){
				data.getWatchedClausoleAppreseNonNegate().get(secondoLet).add(ultimaClausolaAppresa.getNumeroClausola());
			}
			else{
				data.getWatchedClausoleAppreseNegate().get(-secondoLet).add(ultimaClausolaAppresa.getNumeroClausola());
			}
		}
		
		daPropagare.clear();
		daPropagareHash.clear();
		
		aggiungiDaPropagare(new LetteraleSullaTraccia(c.letterali.get(0), livello_corrente, c));
		
		if(data.mappaClausoleApprese().size() > fattoreOverflow*data.mappaClausole().size()){
			puliziaClausoleAppreseOwerflow();
			if(!cicloEliminazione){
				fattoreOverflow+=1;
				cicloEliminazione = true;
			}
			else{
				fattoreOverflow-=0.5;
				cicloEliminazione = false;
			}
		}
    }	
    
    private ClausolaRisolvente risoluzioneConSussunzione(Clausola c1, Clausola c2){
        ClausolaRisolvente risultato = new ClausolaRisolvente();
        for(int let2: c1.letterali){
            if(!c2.presenteNegato(let2))
                risultato.aggiungiLetterale(let2);
        }
        for(int let2: c2.letterali){
            if(!c1.presenteNegato(let2) && !risultato.presente(let2)) //la seconda condizione serve a non generare duplicati
                risultato.aggiungiLetterale(let2);
        }
		if(generaProva)
			risultato.setPadri(c1, c2);
		return risultato;
    }

    public void resetAndCreateStructure(File f){
        System.out.println("RESET");
        reset();
		inter.computazioneNonCompletata();
        try {
            createStructure(f);
        } catch (Exception ex) {
            Logger.getLogger(Core.class.getName()).log(Level.SEVERE, null, ex);
			exit(1);
        }
    }
    
    private void reset(){
        traccia.clear();
        livello_corrente = 0;
        finito = false;
        soddisfatto = false;
        ultimaClausolaAppresa = null;
		numVar = 0;
		numClausole = 0;
		contatorePerVSIDS = 0;
		variabile_selezionata = -1;
		soddisfatto = false; 
		ultimaClausolaAppresa = null;
		assegnamentoScore = 1;
		listaRis = new LinkedList<>();
		interruzioneEsterna = false;
		tracciaRisultato = "";
		contatoreRestart = 0;
		daPropagare.clear();
		daPropagareHash.clear();
		prova.clear();
		stopGenerazioneProva = false;
		sequenzaRestart = new int[7];
		sequenzaRestart[0] = 100;
		sequenzaRestart[1] = 100;
		sequenzaRestart[2] = 200;
		sequenzaRestart[3] = 100;
		sequenzaRestart[4] = 100;
		sequenzaRestart[5] = 200;
		sequenzaRestart[6] = 400;
		posSequenzaRestart = 0;
		numConflitti = 0;
		contPropagazioni = 0;
		numRestart=0;
		tracciaUltimaSemplificazione = 0;
		clausoleAppreseEliminate = 0;
		dimClausoleCondivisePrima = 0;
		clausoleSemplificate = 0;
		clausoleSussunte = 0;
    }
    
    private void createStructure(File f) {
		System.out.println("Creazione struttura");
        try (BufferedReader br = new BufferedReader(new FileReader(f))) {
            String line;
            String token;
            boolean iniziato = false;
            StringTokenizer st;
            while ((line = br.readLine()) != null && !iniziato) {
                st = new StringTokenizer(line);
                if(st.nextToken().equals("p")){
                    if(st.nextToken().equals("cnf")){
                        numVar = Integer.parseInt(st.nextToken());
                        numClausole = Integer.parseInt(st.nextToken());
                        data = new StrutturaDati(numVar, numClausole, inter);
                        iniziato = true;
                    }    
                }
            }
            if(iniziato){
                int i=0;
                while(i<numClausole){
                    if(line != null){
                        st = new StringTokenizer(line);
                        while(st.hasMoreTokens()){
                            token = st.nextToken();
                            if(!token.equals("0")){
                                data.addData(Integer.parseInt(token), i+1); //uso i+1 per contare le clausole da 1 e non da 0
                            }
                            else{
                                i++;
                            }
                        }
                    }
					else{
						System.err.println("Il file presenta problemi: non è presente il contenuto di tutte le clausole");
						throw new Exception();
					}
                    line = br.readLine(); 
                }
                inter.fineLettura();
                
            }
            else{
                System.err.println("File non supportato");
                throw new Exception();
            }
            
        }
        catch(Exception e){
			inter.impossibileLeggereIlFile();
        }
        eliminaTautologie();
		data.setVeroNumeroVar();
    }
	
	private void eliminaTautologie(){
		HashSet<Integer> intersezione;
		for(int i=1; i< data.variabili().length;i++){
			if(data.mappaVariabiliNonNegate().get(i) != null){
				if(data.mappaVariabiliNegate().get(i) != null){
					intersezione = new HashSet<Integer>(data.mappaVariabiliNonNegate().get(i));
					intersezione.retainAll(data.mappaVariabiliNegate().get(i));
					if(!intersezione.isEmpty()){
						for(Integer clausola: intersezione){
							//System.out.println("Eliminazione tautologia "+data.mappaClausole().get(clausola).toString());
							data.eliminaClausola(data.mappaClausole().get(clausola));
						}
					}
				}
			}
		}
	}
	
	private void aumentaScore(Clausola c){
		for(int l: c.letterali){
			data.incrementaScoreVariabile(l, assegnamentoScore);
		}
	}
    
    private void printTrace(){
        System.out.println("Traccia: ");
        for(LetteraleSullaTraccia l: traccia){
            System.out.print(l.getValore()+" ");
            if(l.tipo == LetteraleSullaTraccia.DECISO)
                System.out.println(" Letterale Deciso a livello "+l.getLivello());
            else if(l.tipo == LetteraleSullaTraccia.PROPAGATO)
                System.out.println(" Letterale implicato a livello "+l.getLivello()+" con giustificazione "+l.getGiustificazione().toString());
        }
        System.out.println();
    }
    
    private int modulo(int i){
        return i > 0 ? i : -i;
    }
	
	    
    private void restart(){
		numRestart++;
        //System.out.println("RESTART "+nomeProcesso);
        if(traccia.size() > 0){
            if(traccia.getLast().getLivello() > 0){
                while(traccia.getLast().getLivello() > 0){
                    disfaUltimoLetteraleSullaTracciaSenzaAggiungerePropagazioni();
                    if(traccia.size() == 0)
                        break;
                }
            }
        }
        data.resettaScore();
		//puliziaClausoleApprese();
    }
    
	public void interrompiGenerazioneProva(){
		stopGenerazioneProva = true;
	}
	
	/*private void puliziaClausoleApprese(){
		int cont = 0;
		
		for(Integer clausola: data.clausoleAppreseDaEliminareDopoRestart()){
			data.eliminaClausolaAppresa(data.mappaClausoleApprese().get(clausola));
			cont++;
		}
		data.clausoleAppreseDaEliminareDopoRestart().clear();
		clausoleAppreseEliminate+=cont;
	}*/
	
	private void puliziaClausoleAppreseOwerflow(){
		//System.out.println("Pulizia clausole overflow");
		int cont  = 0;
		int target = data.mappaClausoleApprese().size()/2;
		for(int clausola: data.clausoleAppreseDaEliminareDopoRestart()){
			data.eliminaClausolaAppresa(data.mappaClausoleApprese().get(clausola));
			cont++;
			if(cont++ == target){
				break;
			}
		}
		clausoleAppreseEliminate+=cont;
	}
}
