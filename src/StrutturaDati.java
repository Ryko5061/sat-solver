import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Viktor
 */
public class StrutturaDati {
    private final GUI inter;
    private final HashMap<Integer, ArrayList<Integer>> var_in_clausole_non_negate; //ogni elemento della mappa è la lista di clausole nella quale è contenuta la variabile 
	private final HashMap<Integer, ArrayList<Integer>> var_in_clausole_negate;
	private final LinkedHashMap<Integer, Clausola> clause_with_literals = new LinkedHashMap<>(); //ogni elemento della mappa è la lista di variabili della clausola
    private final LinkedHashMap<Integer, ClausolaRisolvente>  clause_with_literals_apprese = new LinkedHashMap<>(); 
	private static final LinkedHashSet<ClausolaRisolventeCondivisa>  clausole_condivise_nuove = new LinkedHashSet<>(); 
    private final HashMap<Integer, ArrayList<Integer>> var_in_clausole_apprese_non_negate = new HashMap<>();
	private final HashMap<Integer, ArrayList<Integer>> var_in_clausole_apprese_negate = new HashMap<>();
	private final Variabile[] variabili;
    private final int[] occorrenze_variabili_non_negate;
    private final int[] occorrenze_variabili_negate;
    private final int numVariabili;
    private static int maxNum;
	private final double[] score;
	private final double[] score_negazioni;
	private static final ReentrantLock lock = new ReentrantLock();
	private int last_min_clause = Integer.MAX_VALUE;
	private final LinkedList<Integer> clausoleAppreseDaEliminareDopoRestart = new LinkedList<>();
	private final LinkedList<Integer> clausoleNuove = new LinkedList<>();
	private final LinkedList<Clausola> daEliminare = new LinkedList<>();
	private final HashMap<Integer, ArrayList<Integer>> watched_in_clausole_non_negate =new HashMap<>();
	private final HashMap<Integer, ArrayList<Integer>> watched_in_clausole_negate = new HashMap<>();
	private final HashMap<Integer, ArrayList<Integer>> watched_in_clausole_apprese_non_negate = new HashMap<>();
	private final HashMap<Integer, ArrayList<Integer>> watched_in_clausole_apprese_negate = new HashMap<>();
	private int veroNumeroVar;
	private boolean ricalcoloOccorrenzeFast = false;
	
	public Variabile[] variabili(){
		return variabili;
	}
	
	public int veroNumeroVar(){
		return veroNumeroVar;
	}
	
	public HashMap<Integer, ArrayList<Integer>> getWatchedClausoleNegate(){
		return watched_in_clausole_negate;
	}
	
	public HashMap<Integer, ArrayList<Integer>> getWatchedClausoleNonNegate(){
		return watched_in_clausole_non_negate;
	}
	
	public HashMap<Integer, ArrayList<Integer>> getWatchedClausoleAppreseNegate(){
		return watched_in_clausole_apprese_negate;
	}
	
	public HashMap<Integer, ArrayList<Integer>> getWatchedClausoleAppreseNonNegate(){
		return watched_in_clausole_apprese_non_negate;
	}
	
	public LinkedList<Integer> clausoleAppreseDaEliminareDopoRestart(){
		return clausoleAppreseDaEliminareDopoRestart;
	}
	
    public StrutturaDati(int numVar, int numC, GUI inter){
		if(lock.isLocked())
			lock.unlock();
        this.inter = inter;
        numVariabili = numVar;
        var_in_clausole_negate = new HashMap<>();
		var_in_clausole_non_negate = new HashMap<>();
        variabili = new Variabile[numVariabili+1];
        occorrenze_variabili_negate = new int[numVariabili+1];
        occorrenze_variabili_non_negate = new int[numVariabili+1];       
		score = new double[numVariabili+1];
		score_negazioni = new double[numVariabili+1];
		Arrays.fill(score, 0);
		Arrays.fill(score_negazioni, 0);
        maxNum = numC;
		System.out.println("numero variabili: "+numVar+"\nnumero clausole: "+numC);		
    }
    
    public Map<Integer, Clausola> mappaClausole(){
        return clause_with_literals;
    }	
	
	public int sussunzione(){
        //System.out.println("SUSSUNZIONE");
        int clausoleSussunte = 0;
        LinkedList<Integer> daEliminareSussunzione = new LinkedList<>();
		
		Clausola[] clausole = new Clausola[clause_with_literals.values().size()];
		clause_with_literals.values().toArray(clausole);
		Arrays.sort(clausole);
		
		for(int i = 0; i < clausole.length;i++){
			for(int k = i+1; k < clausole.length; k++){
				if(clausole[k].contiene(clausole[i])){
					//System.out.println("Sussunzione: "+"clausola num "+clausole[i].getNumeroClausola()+": "+clausole[i].toString()+" sussume "+clausole[k].getNumeroClausola()+": "+clausole[k].toString());
					clausoleSussunte++;
					daEliminareSussunzione.add(clausole[k].getNumeroClausola());
				}
			}
		}
		
		for(Integer cla: daEliminareSussunzione){
			eliminaClausola(clause_with_literals.get(cla));
		}
		return clausoleSussunte;
		//System.out.println("Sussunzione completata");
    }
	
	public int sussunzione(ClausolaRisolvente cla){
		int cont = 0;
        //computazionalmente lento, meglio dei 2 cicli for annidati
		for(Clausola c: mappaClausole().values()){
            if(c.contiene(cla)){
				cont++;
				daEliminare.add(c);
            }
		}
		
		for(Clausola c: daEliminare){
			eliminaClausola(c);
		}	
		
		daEliminare.clear();
		
		/*for(ClausolaRisolvente c: mappaClausoleApprese().values()){
			if(!cla.equals(c)){
                if(c.contiene(cla)){
					daEliminare.add(c);
                }
			}
		}
		
		for(Clausola c: daEliminare){
			eliminaClausolaAppresa((ClausolaRisolvente) c);
		}	
		
		daEliminare.clear();*/
		return cont;
	}
    
    public Map<Integer, ClausolaRisolvente> mappaClausoleApprese(){
        return clause_with_literals_apprese;
    }
    
    public Map<Integer, ArrayList<Integer>> mappaVariabiliNegate(){
        return var_in_clausole_negate;
    }
	
	public Map<Integer, ArrayList<Integer>> mappaVariabiliNonNegate(){
        return var_in_clausole_non_negate;
    }
	
	public void setVeroNumeroVar(){
		int cont = 0;
		for(int i = 1; i< variabili.length;i++){
			if(variabili[i] != null)
				cont ++;
		}
		veroNumeroVar = cont;
	}
    
    public void addData(int var, int clause){
        //System.out.println("var: "+var);
        int modulo = var;
        if(var < 0) modulo = -var;
        variabili[modulo] = new Variabile();
        if(!clause_with_literals.containsKey(clause))
            clause_with_literals.put(clause, new Clausola(clause));
		
		if(!var_in_clausole_non_negate.containsKey(modulo)){
			var_in_clausole_non_negate.put(modulo, new ArrayList<>());		
		}
	
		if(!var_in_clausole_negate.containsKey(modulo)){
			var_in_clausole_negate.put(modulo, new ArrayList<>());
		}
		
		var_in_clausole_apprese_negate.put(modulo, new ArrayList<>());
		var_in_clausole_apprese_non_negate.put(modulo, new ArrayList<>());
		watched_in_clausole_negate.put(modulo, new ArrayList<>());
		watched_in_clausole_non_negate.put(modulo, new ArrayList<>());
		watched_in_clausole_apprese_negate.put(modulo, new ArrayList<>());
		watched_in_clausole_apprese_non_negate.put(modulo, new ArrayList<>());
		if(!clause_with_literals.get(clause).contiene(var))
			clause_with_literals.get(clause).aggiungiLetterale(new Letterale(var));
		if(var > 0){
			if(!var_in_clausole_non_negate.get(modulo).contains(clause)){
				var_in_clausole_non_negate.get(modulo).add(clause);
			}
			else{
				//System.out.println("Non aggiungo il duplicato "+var);
			}
		}
		else{
			if(!var_in_clausole_negate.get(modulo).contains(clause)){
				var_in_clausole_negate.get(modulo).add(clause);
			}
			else{
				//System.out.println("Non aggiungo il duplicato "+var);
			}
		}
        if(inter != null)
            inter.activateStartButton();
    }
	
	public int getNextAssegnamentoVariabile(int euristica){
		switch(euristica){
			case (Core.EURISTICA_VSIDS):		return getNextAssegnamentoVariabileVSIDS();
			case (Core.EURISTICA_SCF):		return getNextAssegnamentoVariabileSCF();
			case (Core.EURISTICA_GREEDY):	return getNextAssegnamentoVariabile();
		}
		return 0;
	}
	
	//ritorna la la variabile negata se l'assegnamento deve essere a 0
    private int getNextAssegnamentoVariabileSCF(){
        int min = Integer.MAX_VALUE;
        int variabile = 0;
        
        for(int i=1;i<variabili.length;i++){
			if(variabili[i] != null){
				if(variabili[i].getAssegnamento() == Variabile.NON_ASSEGNATO){
					for(int c: var_in_clausole_apprese_negate.get(i)){
						ClausolaRisolvente cla = clause_with_literals_apprese.get(c);
						if(!soddisfattaWatched(cla)){
							int temp_min = cla.letterali.size();
							if(temp_min < min){
								min = temp_min;
								variabile = -i;
								//ho trovato un risultato ottimale
								if(min == last_min_clause){
									return variabile;
								}
							}
						}
					}
					for(int c: var_in_clausole_apprese_non_negate.get(i)){
						ClausolaRisolvente cla = clause_with_literals_apprese.get(c);
						if(!soddisfattaWatched(cla)){
							int temp_min = cla.letterali.size();
							if(temp_min < min){
								min = temp_min;
								variabile = i;
								//ho trovato un risultato ottimale
								if(min == last_min_clause){
									return variabile;
								}	
							}
						}
					}
					for(int c: var_in_clausole_non_negate.get(i)){
						Clausola cla = clause_with_literals.get(c);
						if(!soddisfattaWatched(cla)){
							int temp_min = cla.letterali.size();
							if(temp_min < min){
								min = temp_min;
								variabile = i;
								//ho trovato un risultato ottimale
								if(min == last_min_clause){
									return variabile;
								}
							}
						}
					}
					for(int c: var_in_clausole_negate.get(i)){
						Clausola cla = clause_with_literals.get(c);
						if(!soddisfattaWatched(cla)){
							int temp_min = cla.letterali.size();
							if(temp_min < min){
								min = temp_min;
								variabile = -i;
								//ho trovato un risultato ottimale
								if(min == last_min_clause){
									return variabile;
								}
							}
						}		
					}
				}
			}
        }
		
		/*if(variabile == 0){
			try {
				throw new Exception("Variabile non valida");
			} catch (Exception ex) {
				Logger.getLogger(StrutturaDati.class.getName()).log(Level.SEVERE, null, ex);
				exit(1);
			}
		}*/
		
        //System.out.println("variabile: "+variabile+" con "+max+" occorrenze");
        last_min_clause = min;
        return variabile;
    }
	
	/*private int getNextAssegnamentoVariabileVSIDSSCF(){
        int min = Integer.MAX_VALUE;
        int variabile = 0;
		double max_score = ricalcolaOccorrenzeVSIDS();
        
        for(int i=1;i<variabili.length;i++){
			if(variabili[i] != null){
				if(variabili[i].getAssegnamento() == Variabile.NON_ASSEGNATO){
					if(score[i] == max_score){
						for(int c: var_in_clausole_apprese_non_negate.get(i)){
							ClausolaRisolvente cla = clause_with_literals_apprese.get(c);
							if(!soddisfattaWatched(cla)){
								int temp_min = cla.letterali.size();
								if(temp_min < min){
									min = temp_min;
									variabile = i;
									//ho trovato un risultato ottimale
									if(min == last_min_clause){
										return variabile;
									}	
								}
							}
						}
						for(int c: var_in_clausole_non_negate.get(i)){
							Clausola cla = clause_with_literals.get(c);
							if(!soddisfattaWatched(cla)){
								int temp_min = cla.letterali.size();
								if(temp_min < min){
									min = temp_min;
									variabile = i;
									//ho trovato un risultato ottimale
									if(min == last_min_clause){
										return variabile;
									}
								}
							}
						}
						for(int c: var_in_clausole_apprese_negate.get(i)){
							ClausolaRisolvente cla = clause_with_literals_apprese.get(c);
							if(!soddisfattaWatched(cla)){
								int temp_min = cla.letterali.size();
								if(temp_min < min){
									min = temp_min;
									variabile = -i;
									//ho trovato un risultato ottimale
									if(min == last_min_clause){
										return variabile;
									}
								}
							}
						}
						for(int c: var_in_clausole_negate.get(i)){
							Clausola cla = clause_with_literals.get(c);
							if(!soddisfattaWatched(cla)){
								int temp_min = cla.letterali.size();
								if(temp_min < min){
									min = temp_min;
									variabile = -i;
									//ho trovato un risultato ottimale
									if(min == last_min_clause){
										return variabile;
									}
								}
							}		
						}
					}
					else if(score_negazioni[i] == max_score){
						for(int c: var_in_clausole_apprese_negate.get(i)){
							ClausolaRisolvente cla = clause_with_literals_apprese.get(c);
							if(!soddisfattaWatched(cla)){
								int temp_min = cla.letterali.size();
								if(temp_min < min){
									min = temp_min;
									variabile = -i;
									//ho trovato un risultato ottimale
									if(min == last_min_clause){
										return variabile;
									}
								}
							}
						}
						for(int c: var_in_clausole_negate.get(i)){
							Clausola cla = clause_with_literals.get(c);
							if(!soddisfattaWatched(cla)){
								int temp_min = cla.letterali.size();
								if(temp_min < min){
									min = temp_min;
									variabile = -i;
									//ho trovato un risultato ottimale
									if(min == last_min_clause){
										return variabile;
									}
								}
							}		
						}
						for(int c: var_in_clausole_apprese_non_negate.get(i)){
							ClausolaRisolvente cla = clause_with_literals_apprese.get(c);
							if(!soddisfattaWatched(cla)){
								int temp_min = cla.letterali.size();
								if(temp_min < min){
									min = temp_min;
									variabile = i;
									//ho trovato un risultato ottimale
									if(min == last_min_clause){
										return variabile;
									}	
								}
							}
						}
						for(int c: var_in_clausole_non_negate.get(i)){
							Clausola cla = clause_with_literals.get(c);
							if(!soddisfattaWatched(cla)){
								int temp_min = cla.letterali.size();
								if(temp_min < min){
									min = temp_min;
									variabile = i;
									//ho trovato un risultato ottimale
									if(min == last_min_clause){
										return variabile;
									}
								}
							}
						}
					}
				}
			}
        }
		
		if(variabile == 0){
			try {
				throw new Exception("Variabile non valida");
			} catch (Exception ex) {
				Logger.getLogger(StrutturaDati.class.getName()).log(Level.SEVERE, null, ex);
				exit(1);
			}
		}
		
        //System.out.println("variabile: "+variabile+" con "+max+" occorrenze");
        last_min_clause = min;
        return variabile;
    }*/
	
    //ritorna la variabile negata se l'assegnamento deve essere a 0
    private int getNextAssegnamentoVariabile(){
		ricalcolaOccorrenze();
        int max = Integer.MIN_VALUE;
        int variabile = 0;
        
        for(int i=1;i<variabili.length;i++){
			//certi numeri possono non essere stati utilizzati nel file letto
			if(variabili[i] != null){	
				if(variabili[i].getAssegnamento() == Variabile.NON_ASSEGNATO){
					if(occorrenze_variabili_non_negate[i] > max){
						max = occorrenze_variabili_non_negate[i];
						variabile = i;
					}
					if(occorrenze_variabili_negate[i] > max){
						max = occorrenze_variabili_negate[i];
						variabile = -i;
					}
				}
			}
        }	  
        return variabile;
    }
	
	private boolean soddisfattaWatched(Clausola c){
        int var = c.letterali.get(0);
		int modulo_var = modulo(var);
		if(!variabileNonAssegnata(modulo_var)){
			if(var > 0){
			    if(variabili()[modulo_var].getAssegnamento() == Variabile.UNO)
					return true;
			}
			else{
			    if(variabili()[modulo_var].getAssegnamento() == Variabile.ZERO)
					return true;
			}
		}
		if(c.letterali.size() > 1){
			var = c.letterali.get(1);
			modulo_var = modulo(var);
			if(!variabileNonAssegnata(modulo_var)){
				if(var > 0){
					return variabili()[modulo_var].getAssegnamento() == Variabile.UNO;
				}
				else{
					return variabili()[modulo_var].getAssegnamento() == Variabile.ZERO;
				}
			}
		}
        return false;
	}
	
	public void incrementaScoreVariabile(int variabile, int quantita){
		if(variabile > 0)
			score_negazioni[variabile]+= quantita;
		else
			score[-variabile]+= quantita;
	}
	
	public void dimezzaTuttiGliScore(){
		for(int i=0;i< score.length;i++){
			score[i] = 2;
			score_negazioni[i]/=2;
		}
	}
	
	//ritorna la la variabile negata se l'assegnamento deve essere a 0
    private int getNextAssegnamentoVariabileVSIDS(){
        int max = Integer.MIN_VALUE;
        int variabile = 0;
		
		double max_score = ricalcolaOccorrenzeVSIDS();
        
        for(int i=1;i<variabili.length;i++){
			//certi numeri possono non essere stati utilizzati nel file letto
			if(variabili[i] != null){	
				if(variabili[i].getAssegnamento() == Variabile.NON_ASSEGNATO){
					if(score[i] == max_score){
						if(occorrenze_variabili_non_negate[i] > max){
							max = occorrenze_variabili_non_negate[i];
							variabile = i;
						}
					}
					else if(score_negazioni[i] == max_score){
						if(occorrenze_variabili_negate[i] > max){
							max = occorrenze_variabili_negate[i];
							variabile = -i;
						}
					}
				}
			}
        }
		
		/*if(variabile == 0){
			for(int i=1;i<variabili.length;i++){
				//dei numeri delle variabili potrebbero non essere utilizzati
				if(variabili[i] != null){
					if(variabili[i].getAssegnamento() == Variabile.NON_ASSEGNATO){
						System.out.println("Var non assegnata: "+i);
					}
				}
			}
			try {
				throw new Exception("Variabile non valida");
			} catch (Exception ex) {
				Logger.getLogger(StrutturaDati.class.getName()).log(Level.SEVERE, null, ex);
				exit(1);
			}
		}*/
        return variabile;
    }
	
	public boolean variabileNonAssegnata(int valore_assoluto_variabile){
		return variabili[valore_assoluto_variabile].getAssegnamento() == Variabile.NON_ASSEGNATO;
	}
	
	public boolean variabileOk(int letterale) {
		return variabili[modulo(letterale)].getAssegnamento() == Variabile.NON_ASSEGNATO || variabileSoddisfa(letterale);
	}
	
	public boolean variabileSoddisfa(int letterale){
		if(letterale > 0)
			return variabili[letterale].getAssegnamento() == Variabile.UNO;
		else
			return variabili[-letterale].getAssegnamento() == Variabile.ZERO;
	}
	
    //da vedere la variante rispetto alle variabili e non iterando sulle clausole
	private void ricalcolaOccorrenze(){
		Arrays.fill(occorrenze_variabili_negate, 0);
		Arrays.fill(occorrenze_variabili_non_negate, 0);
  
		for ( Clausola c : clause_with_literals.values()) {
			if(!clausolaSoddisfatta(c)){
				for(int l: c.letterali){
					if(variabili[modulo(l)].getAssegnamento() == Variabile.NON_ASSEGNATO){
						if(l < 0){
							occorrenze_variabili_negate[modulo(l)]++;
						}
						else{
							occorrenze_variabili_non_negate[modulo(l)]++;
						}
					}
				}
			}
		}

		for ( Clausola c : clause_with_literals_apprese.values()) {
			if(!clausolaSoddisfatta(c)){
				for(int l: c.letterali){
					if(variabili[modulo(l)].getAssegnamento() == Variabile.NON_ASSEGNATO){
						if(l < 0){
							occorrenze_variabili_negate[modulo(l)]++;
						}
						else{
							occorrenze_variabili_non_negate[modulo(l)]++;						    
						}
					}
				}
			}
		}		
    }
	
	private double ricalcolaOccorrenzeVSIDS(){
		Arrays.fill(occorrenze_variabili_negate, 0);
		Arrays.fill(occorrenze_variabili_non_negate, 0);
		double max_score = 0;
		for(int i = 1; i< score.length;i++){
			if(variabili[i] != null){
				if(variabili[i].getAssegnamento() == Variabile.NON_ASSEGNATO){
					if(score[i] > max_score)
						max_score = score[i];
					if(score_negazioni[i] > max_score)
						max_score = score_negazioni[i];
				}
			}
		}
  
		for(int i=1; i< variabili.length;i++){
			if(variabili[i] != null){
				if(variabili[i].getAssegnamento() == Variabile.NON_ASSEGNATO){
					if(score[i] == max_score){
						for(int clausola: var_in_clausole_apprese_non_negate.get(i)){
							if(ricalcoloOccorrenzeFast){
								if(!soddisfattaWatched(clause_with_literals_apprese.get(clausola))){
									occorrenze_variabili_non_negate[i]++;
								}
							}
							else{
								if(!clausolaSoddisfatta(clause_with_literals_apprese.get(clausola))){
									occorrenze_variabili_non_negate[i]++;
								}
							}
						}
						
						for(int clausola: var_in_clausole_non_negate.get(i)){
							if(ricalcoloOccorrenzeFast){
								if(!soddisfattaWatched(clause_with_literals.get(clausola))){
									occorrenze_variabili_non_negate[i]++;
								}
							}
							else{
								if(!clausolaSoddisfatta(clause_with_literals.get(clausola))){
									occorrenze_variabili_non_negate[i]++;
								}
							}
						}
					}
					
					else if(score_negazioni[i] == max_score){	
						for(int clausola: var_in_clausole_apprese_negate.get(i)){
							if(ricalcoloOccorrenzeFast){
								if(!soddisfattaWatched(clause_with_literals_apprese.get(clausola))){
									occorrenze_variabili_negate[i]++;
								}
							}
							else{
								if(!clausolaSoddisfatta(clause_with_literals_apprese.get(clausola))){
									occorrenze_variabili_negate[i]++;
								}
							}
						}
				
						for(int clausola: var_in_clausole_negate.get(i)){
							if(ricalcoloOccorrenzeFast){
								if(!soddisfattaWatched(clause_with_literals.get(clausola))){
									occorrenze_variabili_negate[i]++;
								}
							}
							else{
								if(!clausolaSoddisfatta(clause_with_literals.get(clausola))){
									occorrenze_variabili_negate[i]++;
								}
							}	
						}
					}
				}
			}
		}
		ricalcoloOccorrenzeFast = !ricalcoloOccorrenzeFast;
		return max_score;
	}
	
	private int modulo(int i){
        return i > 0 ? i : -i;
    }
	
	public boolean clausolaSoddisfatta(Clausola c){
        for(int l: c.letterali){
			int modulo_var = modulo(l);
            if(!variabileNonAssegnata(modulo_var)){
                if(l> 0){
                    if(variabili[modulo_var].getAssegnamento() == Variabile.UNO){
                        return true;
					}
                }
                else{
                    if(variabili[modulo_var].getAssegnamento() == Variabile.ZERO){
                        return true;
					}
                }
            }
        }
        return false;
    }
    
    private boolean clausolaAppresaPresente(Clausola cla){
		for(ClausolaRisolvente c: clause_with_literals_apprese.values()){
			if(c.equals(cla)){
				return true;
			}
		}
		return false;
    }
    
    public void eliminaClausola(Clausola c){
		int l;
		if(c != null){
			for(int i = 0; i< c.letterali.size();i++){
				l = c.letterali.get(i);
				if(l > 0){
					var_in_clausole_non_negate.get(l).remove((Integer) c.getNumeroClausola());
				}
				else{
					var_in_clausole_negate.get(-l).remove((Integer) c.getNumeroClausola());
				}
				if(i < 2){
					if(l > 0){
						getWatchedClausoleNonNegate().get(l).remove((Integer)c.getNumeroClausola());
					}
					else{
						getWatchedClausoleNegate().get(-l).remove((Integer)c.getNumeroClausola());
					}
				}
			}
			clause_with_literals.remove(c.getNumeroClausola());
		}
    }
	
	public void eliminaClausolaAppresa(ClausolaRisolvente c){
		if(c != null){
			int l;
			for(int i= 0;i <  c.letterali.size();i++){
				l = c.letterali.get(i);
				if(l > 0){
					var_in_clausole_apprese_non_negate.get(l).remove((Integer) c.getNumeroClausola());
				}
				else{
					var_in_clausole_apprese_negate.get(-l).remove((Integer) c.getNumeroClausola());
				}
				if(i < 2){
					if(l > 0){
						getWatchedClausoleAppreseNonNegate().get(l).remove((Integer)c.getNumeroClausola());
					}
					else{
						getWatchedClausoleAppreseNegate().get(-l).remove((Integer)c.getNumeroClausola());
					}
				}
			}
			clause_with_literals_apprese.remove(c.getNumeroClausola());
		}
    }
 
    public int aggiungiClausola(ClausolaRisolvente c, String processo, boolean ciSonoAltreThread, int livello){
		int numero = getNum();
		c.setNumero(numero);
		clause_with_literals_apprese.put(numero, c);

		for(int l: c.letterali){
			if(l > 0){
				var_in_clausole_apprese_non_negate.get(l).add(c.getNumeroClausola());
			}
			else{
				var_in_clausole_apprese_negate.get(modulo(l)).add(c.getNumeroClausola());
			}
		}
		if(livello > 0){
			clausoleAppreseDaEliminareDopoRestart.add(c.getNumeroClausola());
		}
		if(ciSonoAltreThread){
			if(livello == 0 && c.letterali.size() > 0){
				lock.lock();
				try{
					if(!clausole_condivise_nuove.contains(c)){
						clausole_condivise_nuove.add(new ClausolaRisolventeCondivisa(c, processo));
						//System.out.println("Io processo "+processo+" aggiungoo alle clausole condivise la clausola "+c.toString());
					}
				}
				finally{
					lock.unlock();
				}
			}
		}
		return clausole_condivise_nuove.size();
    }
	
	public int clausoleNuoveSize(){
		return clausole_condivise_nuove.size();
	}
	
    
    private int getNum(){
        maxNum++;
        return maxNum;
    }

	public int sincronizzaClausoleNuove(String processo){
		//System.out.println("Sincronizzazione clausole nuove "+processo);
		lock.lock();
		try{
			LinkedList<ClausolaRisolventeCondivisa> daEliminareCondivisi = new LinkedList<>();
			for ( ClausolaRisolventeCondivisa c : clausole_condivise_nuove) {
				if(!c.getProcessoCreatore().equals(processo)){
					//forse qui dovrei controllare se la clausola esiste già ma con un altro numero clausola
					//una verifica efficace potrebbe essere la ricerca delle clausole con il primo letterale tra i letterali della clausola
					//poi il secondo e cosi via, facendo l'intersezione tra questi insiemi si trova la clausola che li contiene tutti quindi equivale alla clausola che si vuole aggiungere
					if(Collections.max(c.hashLetterali) <= numVariabili && Collections.min(c.hashLetterali) >= -numVariabili){
						if(!clausolaAppresaPresente(c)){
							//System.out.println("io processo "+processo+" ricevo la clausola "+c.toString()+" dal processo "+c.getProcessoCreatore());
							ClausolaRisolvente cla= new ClausolaRisolvente(c);
							maxNum++;
							cla.setNumero(maxNum);
							clause_with_literals_apprese.put(cla.getNumeroClausola(), cla);
							clausoleNuove.add(cla.getNumeroClausola());
							c.numLettureEseguite++;
							//listaClausoleNuove.add(cla.getNumeroClausola());
					
							for(int l: cla.letterali){
								if(l > 0){
									if(var_in_clausole_apprese_non_negate.get(l) == null){
										var_in_clausole_apprese_non_negate.put(l, new ArrayList<>());
									}
									var_in_clausole_apprese_non_negate.get(l).add(cla.getNumeroClausola());
								}
								else{
									if(var_in_clausole_apprese_negate.get(-l) == null){
										var_in_clausole_apprese_negate.put(-l, new ArrayList<>());
									}
									var_in_clausole_apprese_negate.get(-l).add(cla.getNumeroClausola());
								}
							}
						
							/*if(inter.sussunzioneAttivata()){
								sussunzione(c);
							}*/
						}
						//tutti gli altri processi hanno letto tale clausola
						if(c.numLettureEseguite == 1){
							daEliminareCondivisi.add(c);
						}
					}
					else{
						daEliminareCondivisi.add(c);
					}
				}
			}
			for(ClausolaRisolventeCondivisa clausola: daEliminareCondivisi){
				clausole_condivise_nuove.remove(clausola);
			}
			daEliminareCondivisi.clear();
		}
		finally{
			lock.unlock();
		}
		//return listaClausoleNuove;
		return clausole_condivise_nuove.size();
	}
	
	public LinkedList<Integer> clausoleNuove(){
		return clausoleNuove;
	}
	
	public void resettaScore(){
		for(int i=0;i< score.length;i++){
			score[i] = 0;
			score_negazioni[i] = 0;
		}
	}
}
