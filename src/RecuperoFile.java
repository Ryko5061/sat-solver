
import java.util.logging.Level;
import java.util.logging.Logger;

public class RecuperoFile extends javax.swing.JFrame {
    private Core core1, core2, core3;
    private final GUI gui;
    
    public RecuperoFile(GUI g) {
        initComponents();
        gui = g;
    }
    
    public void setCores(Core c1, Core c2, Core c3){
        core1 = c1;
		core2 = c2;
		core3 = c3;
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();

        fileChooser.addActionListener(evt -> fileChooserActionPerformed());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(fileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(fileChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fileChooserActionPerformed() {//GEN-FIRST:event_fileChooserActionPerformed
     
        if(fileChooser.getSelectedFile() != null){
            try {
                this.setVisible(false);
                gui.inizioLettura();
				core1.resetAndCreateStructure(fileChooser.getSelectedFile());
				core2.resetAndCreateStructure(fileChooser.getSelectedFile());
				core3.resetAndCreateStructure(fileChooser.getSelectedFile());
				gui.setFileName(fileChooser.getName(fileChooser.getSelectedFile()));
            } catch (Exception ex) {
                Logger.getLogger(RecuperoFile.class.getName()).log(Level.SEVERE, null, ex);
            }
                    
        }
    }//GEN-LAST:event_fileChooserActionPerformed

   
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser fileChooser;
    // End of variables declaration//GEN-END:variables
}
