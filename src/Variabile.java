

/**
 *
 * @author Viktor
 */
public class Variabile {
    private int assegnamento;
    public static final int UNO = 1;
    public static final int NON_ASSEGNATO = 0;
    public static final int ZERO = -1;
    
    public Variabile(){
        assegnamento = NON_ASSEGNATO;
    }
    
    public void assegna(int assegnamento){
        this.assegnamento = assegnamento;
    }
    
    public void annulla_assegnamento(){
        assegnamento = NON_ASSEGNATO;
    }
     
    public int getAssegnamento(){
        return assegnamento;
    }




    
}
